#pragma once

#include <vector>
#include <limits>

namespace packer
{
    template<typename T, T minValue, T maxValue>
    constexpr size_t computeSize() noexcept
    {
        static_assert( minValue < maxValue, "Min value should be less then max value" );
        T max = maxValue - minValue;
        size_t result = 0u;
        for( ; max != 0; max /= 2, ++result )
        {
        }
        return result;
    }
}

#define GET_MACRO( _1_, _2_, _3_, _4_, NAME, ...) NAME

#define _DECLARE_FIELD_ERROR( ... ) \
    static_assert(false, "Field cannot be declared with this params");

#define _DECLARE_FIELD_MIN_MAX( _type_, _name_, _minValue_, _maxValue_ ) \
    namespace field_##_name_ { \
        _type_ tag; \
        constexpr _type_ min_##_name_ = _minValue_; \
        constexpr _type_ max_##_name_ = _maxValue_; \
        constexpr size_t size_##_name_ = packer::computeSize<_type_, _minValue_, _maxValue_>(); \
    }

#define _DECLARE_FIELD( _type_, _name_ ) \
    _DECLARE_FIELD_MIN_MAX( _type_, _name_, std::numeric_limits<_type_>::min(), std::numeric_limits<_type_>::max() )

#define DECLARE_FIELD( ... ) GET_MACRO( __VA_ARGS__, _DECLARE_FIELD_MIN_MAX, _DECLARE_FIELD_ERROR, _DECLARE_FIELD, _DECLARE_FIELD_ERROR )( __VA_ARGS__ )

#define USE_FIELD( _name_ ) \
    private: \
        uint64_t _name_ : field_##_name_::size_##_name_; \
    public:  \
        inline decltype( field_##_name_::tag ) get_##_name_() const { return field_##_name_::min_##_name_ + _name_; } \
        inline void set_##_name_( decltype( field_##_name_::tag ) value ) { \
            _name_ = value - field_##_name_::min_##_name_; \
            std::cout << value << " " << field_##_name_::min_##_name_ << " " << value - field_##_name_::min_##_name_ << " " << _name_ << " " << field_##_name_::size_##_name_ << std::endl; \
        }

#define START_PACKED_STRUCTURE( _name_ ) \
    struct _name_ \
    {

#define END_PACKED_STRUCTURE \
        inline std::vector<uint8_t> pack() const \
        { \
            std::vector<uint8_t> result; \
            for (size_t i = sizeof(*this) - 1;; --i) \
            { \
                result.emplace_back(*( reinterpret_cast<const uint8_t*>(this) + i)); \
                if (i == 0) { \
                    break; \
                } \
            } \
            return result; \
        } \
    } __attribute__( ( packed ) );


//usage: 
namespace packed
{
    DECLARE_FIELD( int32_t, abc, -4, -1 )
    DECLARE_FIELD( uint32_t, def, 60, 64 )
    DECLARE_FIELD( bool, ghi )
    
    START_PACKED_STRUCTURE( Packer )
        USE_FIELD( abc )
        USE_FIELD( def )
        USE_FIELD( ghi )
    END_PACKED_STRUCTURE
}

auto packer = packed::Packer();
packer.set_abc( -1 );
packer.set_def( 64 );
packer.set_ghi( true );

std::cout << "sizeof packer: " << sizeof(packer) << ", result: " << packer.get_abc() << std::endl; 
